##Exercice 1
_Base.c_
1. isAPrime(int i) iteratively attempts to divide the int i by every smaller integer
2.
| loops | time      |
| 100   | 15µs      |
| 1000  | 212µs     |
| 10000 | 23881µs   |
| 100000| 1621820µs |
_MeasureTime.c_
1. Le temps d'execution est très variable, de 2000µs a 4000µs
3. For 10000 of value, the internal measure is 16471µs, however the time measure round to 24000µs partly due to the calls around the internal time measurement being counted in the  time command
##Exercice 3
3.
| loops | time      |
| 100   | 301µs     |
| 1000  | 919µs     |
| 10000 | 31599µs   |
| 100000| 446611µs  |
4.
| loops | time      |
| 100   | 176µs     |
| 1000  | 4032µs    |
| 10000 | 4380µs    |
| 100000| 395030µs  |
##Exercice 4
1. Acurate sequencial prime count: 9594
2. The result varie from 3000+ to ~1000
3. privatizing the variable we always end up to 0
4. total count becomes false, and perfs are not improve compared to sequencial
5. Using reduction we go back to the real value
