cmake_minimum_required(VERSION 3.10)
project(TP1_Non_Parallel C )
set(CMAKE_C_STANDARD 11)
set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall")
add_executable(TP1_Non_Parallel main.c base.c ../../Utils/measureTime.c parameters.c ../../Utils/measureTime.h)

cmake_minimum_required(VERSION 3.10)
project(TP1_Parallel C )
set(CMAKE_C_STANDARD 11)

find_package(OpenMP)
if (OPENMP_FOUND)
    set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -fopenmp")
endif()
add_executable(. main.c base.c ../../Utils/measureTime.c parameters.c ../../Utils/measureTime.h)


