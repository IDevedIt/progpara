#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include "../../Utils/measureTime.h"

int ex_2_size = 10000;
int *ex_2_array;
int ex_3_size = 10000;
int *ex_3_array;

void exercice1(int argc, char *argv[]);

void exercice2(int argc, char *argv[]);

void exercice3_1(int argc, char **argv);

void exercice3_2(int argc, char **argv);

void exercice4(int argc, char **argv);
int countThePrime_4() ;

void argError(int argc);

void exercice3_2_run();

int isAPrime(int i) {
    int divider = 2;
    while (divider < i) {
        if (i % divider == 0) {
            return 0;
        }
        divider++;
    }
    return 1;
}

void simpleLoop() {
    for (int i = 0; i < 1000; i++) {
        if (i == 500) {
            printf("I'm halfway there !\n");
        }
    }
    printf("I'm done !\n");
}

void cpuIntensiveLoop(int count) {
#pragma omp parallel for
    for (int i = 0; i < count; i++) {
        isAPrime(i);
    }
}

//This is the main function of your program
//it will be called automatically when you start
int main(int argc, char *argv[]) {
//    exercice1(argc, argv);
//    exercice2(argc, argv);
//    exercice3_1(argc,argv);
//    exercice3_2(argc,argv);
    exercice4(argc, argv);
}

void exercice4(int argc, char **argv) {
    if (argc == 2 ) {
        ex_3_size = atoi(argv[1]);
    }
    ex_3_array = malloc(sizeof(int) * ex_3_size);
    exercice3_2_run();
    printf("primeCount:%d", countThePrime_4());
    free(ex_3_array);
}

int countThePrime_4() {
    int total = 0;
    #pragma omp parallel reduction(+:total)
    for (int i = 0; i < ex_3_size; ++i) {
        total += ex_3_array[i];
    }
    return total;
}

void exercice3_2(int argc, char **argv) {
    if (argc == 2 && 0) {
        ex_3_size = atoi(argv[1]);
    }

    for (int i = 10; i <= 1000000; i *= 10) {
        printf("size: %d\n", i);
        ex_3_size = i;
        ex_3_array = malloc(sizeof(int) * ex_3_size);
        measureTimeAR0(&exercice3_2_run);
        free(ex_3_array);
    }
}

void exercice3_2_run() {
    #pragma omp parallel for
    for (int i = 0; i < ex_3_size; ++i) {
        ex_3_array[i] = isAPrime(i);
    }
}

void exercice3_1(int argc, char **argv) {
    if (argc != 2) {
        argError(argc);
        return;
    }
    measureTimeAR1(&cpuIntensiveLoop, atoi(argv[1]));
}

void exercice2(int argc, char *argv[]) {
    if (argc == 2)
        ex_2_size = atoi(argv[1]);
    ex_2_array = malloc((size_t) ex_2_size);
    for (int i = 0; i < ex_2_size; ++i) {
        printf("%d \n", ex_2_array[i]);
    }
    free(ex_2_array);
}

void exercice1(int argc, char *argv[]) {//There is always one argument that is the name of the program
    printf("You have started %s\n", argv[0]);
    //We check if there are more arguments
    if (argc == 2) {
        measureTimeAR1(&cpuIntensiveLoop, atoi(argv[1]));
    } else {
        argError(argc);
    }
}

void argError(int argc) { printf("Arity Mismatch, Expected 1 given %d", argc - 1); }
