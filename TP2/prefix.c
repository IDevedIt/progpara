#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <math.h>
#include <limits.h>

struct tablo {
    int *tab;
    int size;
};

void printArray(struct tablo *tmp) {
    printf("---- Array of size %i ---- \n", tmp->size);
    int size = tmp->size;
    int i;
    for (i = 0; i < size; ++i) {
        printf("%i ", tmp->tab[i]);
    }
    printf("\n");
}

struct tablo *allocateTablo(int size) {
    struct tablo *tmp = malloc(sizeof(struct tablo));
    tmp->size = size;
    tmp->tab = malloc(size * sizeof(int));
    return tmp;
}

int bringUp(struct tablo *pTablo, struct tablo *destination, int index);


void montee(struct tablo *source, struct tablo *destination) {
    for (int i = 0; i < source->size; ++i) {
        destination->tab[destination->size - i-1] = source->tab[source->size-i-1];
    }
    printArray(destination);
    #pragma omp parallel for
    for (int j = source->size-1; j >0; --j) {
        int left = destination->tab[2*j];
        int right = destination->tab[2*j+1];
        destination->tab[j] = left+right;
    }
}



void descente(struct tablo *a, struct tablo *b) {
    b->tab[1]=0;
    int left,right;

    for (int i = 1; i < a->size; ++i) {
        left = 2*i;
        right = left+1;
        b->tab[left] = b->tab[i];
        b->tab[right] = b->tab[i] + a->tab[left];
    }
}

void final(struct tablo *a, struct tablo *b) {
    for (int i = b->size/2+1; i < b->size; ++i) {
        b->tab[i] = b->tab[i]+a->tab[i];
    }
}

void generateArray(struct tablo *s) {
    //test array

    s->tab = malloc(s->size * sizeof(int));
    s->tab[0] = 2;
    s->tab[1] = 4;
    s->tab[2] = 5;
    s->tab[3] = 8;
//    s->size = 8;
//    s->tab = malloc(s->size * sizeof(int));
//    s->tab[0] = 3;
//    s->tab[1] = 1;
//    s->tab[2] = 7;
//    s->tab[3] = 0;
//    s->tab[4] = 4;
//    s->tab[5] = 1;
//    s->tab[6] = 6;
//    s->tab[7] = 3;

}

int main(int argc, char **argv) {
    struct tablo source;

    generateArray(&source);

    struct tablo *a = allocateTablo(source.size * 2);
    montee(&source, a);
    printArray(a);

    struct tablo *b = allocateTablo(source.size * 2);
    descente(a, b);
    printArray(b);

    final(a, b);
    printArray(b);
}