##TP2
###I.2 Observation
L'algorithme est beacoup plus lents qu'il ne devrais
###II Professor
You always generate the worst case senario


###Time measures
Sequential
Size        | Time
100         | 17098 µs
1000        | 2071 µs
10000       | 29445 µs
100000      | 209674 µs
1000000     | 3256677 µs
10000000    | 27516371 µs

Paralelle
Size        | Time
100         | 11113 µs
1000        | 2092 µs
10000       | 31844 µs
100000      | 204081 µs
1000000     | 3395975 µs
10000000    | 31482676 µs