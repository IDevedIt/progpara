//
// Created by ThePrecisionBro on 26/02/2019.
//

#ifndef TP1_PARALLEL_MEASURETIME_H
#define TP1_PARALLEL_MEASURETIME_H
void measureTimeAR1(void (*func)(int), int arg);
void measureTimeAR0(void (*func)());
#endif //TP1_PARALLEL_MEASURETIME_H
